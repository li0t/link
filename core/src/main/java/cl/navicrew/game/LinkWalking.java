package cl.navicrew.game;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;

import java.util.HashMap;
import java.util.Map;

public class LinkWalking implements ApplicationListener {
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private TextureAtlas spriteSheet;
    private Array<Sprite> atlasRegions;
    private TextureRegion linkCurrentFrame, bombCurrentFrame;
    Music music;
    private float stateTime;
    Vector2 linkPos, bombPos, exploPos;
    public final float BOMBRATE = 0.2f;
    private Map<Directions, Animation> idles, walk, explosions;
    private boolean bombed, exploding;
    private int activeBombs, bombsLimit, bombLenght;
    private float timeBetweenBombs;
    Directions directionLink, directionExplosion;

    @Override
    public void create() {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        batch = new SpriteBatch();
        linkPos = new Vector2();
        linkPos.x = 350;
        linkPos.y = 210;
        idles = new HashMap<Directions, Animation>();
        walk = new HashMap<Directions, Animation>();
        explosions = new HashMap<Directions, Animation>();
        directionLink = Directions.DOWN;
        bombed = false;
        exploding = false;
        activeBombs = 0;
        bombsLimit = 1;
        directionExplosion = Directions.CENTER;
        bombLenght = 1;
        music = Gdx.audio.newMusic(Gdx.files.internal("music/zelda Overworld BGM.mp3"));
        loadResources();
        music.setLooping(true);
        music.play();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix(camera.combined);

        stateTime += Gdx.graphics.getDeltaTime();
        timeBetweenBombs += stateTime;
        linkCurrentFrame = idles.get(directionLink).getKeyFrame(stateTime);

        //movement
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            linkPos.x -= 250 * Gdx.graphics.getDeltaTime();
            linkCurrentFrame = walk.get(Directions.LEFT).getKeyFrame(stateTime);
            directionLink = Directions.LEFT;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            linkPos.x += 250 * Gdx.graphics.getDeltaTime();
            linkCurrentFrame = walk.get(Directions.RIGHT).getKeyFrame(stateTime);
            directionLink = Directions.RIGHT;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            linkPos.y += 250 * Gdx.graphics.getDeltaTime();
            linkCurrentFrame = walk.get(Directions.UP).getKeyFrame(stateTime);
            directionLink = Directions.UP;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            linkPos.y -= 250 * Gdx.graphics.getDeltaTime();
            linkCurrentFrame = walk.get(Directions.DOWN).getKeyFrame(stateTime);
            directionLink = Directions.DOWN;
        }

        //borders
        if (linkPos.x < 0) linkPos.x = 0;
        if (linkPos.x > 800 - 64) linkPos.x = 800 - 60;
        if (linkPos.y < 0) linkPos.y = 0;
        if (linkPos.y > 480 - 50) linkPos.y = 480 - 50;

        //bombs
        //if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            if (timeBetweenBombs <= BOMBRATE && activeBombs <= bombsLimit) {
                bombed = true;
                activeBombs++;
                bombPos = linkPos;
                exploPos = bombPos;

                final Bomb bomb = new Bomb(new Texture(Gdx.files.internal("player/bomb_idle.png")),
                        bombPos.x, bombPos.y, bombLenght, 1);
                bombCurrentFrame = bomb.currentFrame;
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        exploding = true;
                        bombed = false;
                        activeBombs--;
                    }
                }, 4);

            }
       // }


        batch.begin();
        batch.draw(linkCurrentFrame, linkPos.x, linkPos.y);
        if (bombed)
            batch.draw(bombCurrentFrame, bombPos.x, bombPos.y);
        if (exploding) {
            for (Map.Entry<Directions, Animation> explosion : explosions.entrySet())
                batch.draw(explosions.get(directionExplosion).getKeyFrame(stateTime), exploPos.x, exploPos.y);
        }

        batch.end();


    }


    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    private void loadResources() {
        spriteSheet = new TextureAtlas(Gdx.files.internal("linkwalking.txt"));

        //linkidleup
        atlasRegions = spriteSheet.createSprites("linkidleup");
        idles.put(Directions.UP, new Animation(0.35f, atlasRegions, Animation.PlayMode.LOOP));

        //linkidleleft
        atlasRegions = spriteSheet.createSprites("linkidleleft");
        idles.put(Directions.LEFT, new Animation(0.35f, atlasRegions, Animation.PlayMode.LOOP));

        //linkidleright
        atlasRegions = spriteSheet.createSprites("linkidleright");
        idles.put(Directions.RIGHT, new Animation(0.35f, atlasRegions, Animation.PlayMode.LOOP));

        //linkidledown
        atlasRegions = spriteSheet.createSprites("linkidledown");
        idles.put(Directions.DOWN, new Animation(0.35f, atlasRegions, Animation.PlayMode.LOOP));

        //linkwalkingup
        atlasRegions = spriteSheet.createSprites("linkwalkingup");
        walk.put(Directions.UP, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //linkwalkingleft
        atlasRegions = spriteSheet.createSprites("linkwalkingleft");
        walk.put(Directions.LEFT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //linkwalkingright
        atlasRegions = spriteSheet.createSprites("linkwalkingright");
        walk.put(Directions.RIGHT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //linkwalkingdown
        atlasRegions = spriteSheet.createSprites("linkwalkingdown");
        walk.put(Directions.DOWN, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));


        //explosions
        spriteSheet = new TextureAtlas(Gdx.files.internal("effects/explosion.txt"));

        //explosioncenter
        atlasRegions = spriteSheet.createSprites("explosioncenter");
        explosions.put(Directions.CENTER, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //explosionup
        atlasRegions = spriteSheet.createSprites("explosionup");
        explosions.put(Directions.UP, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //explosiondown
        atlasRegions = spriteSheet.createSprites("explosiondown");
        explosions.put(Directions.DOWN, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //explosionleft
        atlasRegions = spriteSheet.createSprites("explosionleft");
        explosions.put(Directions.LEFT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //explosionright
        atlasRegions = spriteSheet.createSprites("explosionright");
        explosions.put(Directions.RIGHT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //explosionvertical
        atlasRegions = spriteSheet.createSprites("explosionvertical");
        explosions.put(Directions.VERTICAL, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //explosionhorizontal
        atlasRegions = spriteSheet.createSprites("explosionhorizontal");
        explosions.put(Directions.HORIZONTAL, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));


    }

}
