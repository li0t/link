package cl.navicrew.game;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;


public class Bomb {

    Animation animation;
    private final int FRAMES = 3;
    float stateTime;
    boolean hasExploded;
    int length;
    int strength;
    public Map explosions;
    boolean dead;
    Sound sound;
    Vector2 position;
    TextureRegion currentFrame;
    Rectangle rect;


    /**
     * @param texture La textura inicial de una bomba
     * @param x       La posición x
     * @param y       La posición y
     */
    public Bomb(Texture texture, float x, float y, int length, int strength) {
        currentFrame = new TextureRegion(texture);
        position = new Vector2(x, y);
        rect = new Rectangle(x, y, texture.getWidth(), texture.getHeight());
        this.length = length;
        this.strength = strength;

        // Carga la animación de un spritesheet (todos los frames están en un mismo fichero)
        Texture spriteSheet = new Texture(Gdx.files.internal("player/bomb_animation.png"));
        TextureRegion[][] frames = TextureRegion.split(spriteSheet, spriteSheet.getWidth() / FRAMES, spriteSheet.getHeight());
        TextureRegion[] rightFrames = new TextureRegion[FRAMES];
        for (int i = 0; i < FRAMES; i++) {
            rightFrames[i] = frames[0][i];
        }
        animation = new Animation(0.15f, rightFrames);
        stateTime = 0;
        dead = false;
    }

    /**
     * Hace detonar la bomba
     */
    public void explode(Explosion explosion) {

        hasExploded = true;
        Sound sound = Gdx.audio.newSound(Gdx.files.internal("bomb.wav"));
        sound.play();

    }

    /**
     * Marca la bomba para desaparecer
     */
    public void die() {
        dead = true;
    }

    /**
     * Comprueba si la bomba está marcada para desaparecer
     *
     * @return
     */
    public boolean isDead() {
        return dead;
    }


    public void render(SpriteBatch batch) {

    }


    public void update(float dt) {


    }
}
