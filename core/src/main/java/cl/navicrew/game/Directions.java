package cl.navicrew.game;

import java.io.Serializable;

/**
 * Created by liot on 8/11/14.
 */
public enum Directions implements Serializable {
    UP(1 , "UP"),
    DOWN(2 , "DOWN"),
    LEFT(3 , "LEFT"),
    RIGHT(4 , "RIGHT"),
    CENTER(5 , "CENTER"),
    HORIZONTAL(6 , "HORIZONTAL"),
    VERTICAL(7 , "VERTICAL" );

    private final int id;
    private final String label;

    private Directions(int id, String label) {
        this.id = id;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public static Directions getEnum(int id) {
        for (Directions e : Directions.values()) {
            if (e.getId() == id) {
                return e;
            }
        }
        return null;
    }

    public static Directions getEnum(String name) {
        for (Directions e : Directions.values()) {
            if (e.getLabel().equals(name)) {
                return e;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return label;
    }
}

