package cl.navicrew.game;


import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * Created by liot on 10/13/14.
 */
public class Main {
    public static void main(String[] args) {
        LwjglApplicationConfiguration configuration = new LwjglApplicationConfiguration();
        configuration.title = "LinkWalking";
        configuration.resizable = false;
        configuration.fullscreen = false;

        new LwjglApplication(new LinkWalking(), configuration);
    }
}
